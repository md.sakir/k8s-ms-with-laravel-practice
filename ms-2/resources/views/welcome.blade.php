<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MS 2</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <style>
        body,
        html {
            height: 100%;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100%;
        }

        .text-center {
            font-size: 4rem;
            transition: transform 0.3s ease;
        }

        .text-center:hover {
            transform: scale(1.2);
        }
    </style>
</head>

<body>
    @php
        // Retrieve host-related information
        $hostInfo = [
            'Server IP Address' => $_SERVER['SERVER_ADDR'],
            'Hostname' => gethostname(),
            'Server Name' => $_SERVER['SERVER_NAME'],
            'User Agent' => $_SERVER['HTTP_USER_AGENT'],
            'Server Software' => $_SERVER['SERVER_SOFTWARE'],
            'Client IP Address' => $_SERVER['REMOTE_ADDR'],
            'Server Port' => $_SERVER['SERVER_PORT'],
            'Server Protocol' => $_SERVER['SERVER_PROTOCOL'],
            'Request Method' => $_SERVER['REQUEST_METHOD'],
            'Request URI' => $_SERVER['REQUEST_URI'],
            'Script Name' => $_SERVER['SCRIPT_NAME'],
            'Script Filename' => $_SERVER['SCRIPT_FILENAME'],
            'Gateway Interface' => $_SERVER['GATEWAY_INTERFACE'],
            'HTTP Host' => $_SERVER['HTTP_HOST'],
            'Document Root' => $_SERVER['DOCUMENT_ROOT'],
        ];
    @endphp

    <table class="table">
        <thead>
            <tr>
                <th>Key</th>
                <th>Value</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($hostInfo as $key => $value)
                <tr>
                    <td>{{ $key }}</td>
                    <td>{{ $value }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="container">
        <div class="text-center">
            Laravel App MS 2
        </div>
    </div>
    <!-- Bootstrap JS (optional) -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</body>

</html>
