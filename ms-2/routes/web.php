<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return [
        'app' => config('app.name'),
        'server_info' => [
            'Server IP Address' => $_SERVER['SERVER_ADDR'],
            'Hostname' => gethostname(),
            'Server Name' => $_SERVER['SERVER_NAME'],
            'User Agent' => $_SERVER['HTTP_USER_AGENT'],
            'Server Software' => $_SERVER['SERVER_SOFTWARE'],
            'Client IP Address' => $_SERVER['REMOTE_ADDR'],
            'Server Port' => $_SERVER['SERVER_PORT'],
            'Server Protocol' => $_SERVER['SERVER_PROTOCOL'],
            'Request Method' => $_SERVER['REQUEST_METHOD'],
            'Request URI' => $_SERVER['REQUEST_URI'],
            'Script Name' => $_SERVER['SCRIPT_NAME'],
            'Script Filename' => $_SERVER['SCRIPT_FILENAME'],
            'Gateway Interface' => $_SERVER['GATEWAY_INTERFACE'],
            'HTTP Host' => $_SERVER['HTTP_HOST'],
            'Document Root' => $_SERVER['DOCUMENT_ROOT'],
        ]
    ];
});