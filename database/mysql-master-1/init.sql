-- Create a user for replication
CREATE USER 'replicator'@'%' IDENTIFIED BY 'secret';
GRANT REPLICATION SLAVE ON *.* TO 'replicator'@'k8s_practice_mysql_slave_1';

FLUSH PRIVILEGES;